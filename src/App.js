import React from "react";

import "./App.css";
//import LoginForm from "./components/HomePage/LoginForm";
import WeatherEngine from "./components/WeatherEngine";

function App() {
  return (
    <div className="App">
      <WeatherEngine location="sydney, au" />
    </div>
  );
}

export default App;
