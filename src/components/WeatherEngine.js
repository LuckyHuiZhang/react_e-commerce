import React, { useEffect, useState } from "react";
import { PulseLoader } from "react-spinners";

import WeatherCard from "./WeatherCard/component";

const WeatherEngine = ({ location }) => {
  /* init for our state variable */
  const [query, setQuery] = useState("");
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [weather, setWeather] = useState({
    temp: null,
    city: null,
    country: null,
    condition: null,
  });

  /* defining the data fetch in function */
  const getWeather = async (q) => {
    setQuery("");
    setLoading(true);
    try {
      const apiRes = await fetch(
        `http://api.openweathermap.org/data/2.5/weather?q=${q}&units=metric&APPID=f4ea5059f8235f1394cbf99a3eed05de`
      );
      const resJSON = await apiRes.json();
      setWeather({
        temp: resJSON.main.temp,
        city: resJSON.name,
        country: resJSON.sys.country,
        condition: resJSON.weather[0].main,
      });
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  /* this hook will make the code run only once the component is mounted and never again */
  useEffect(() => {
    getWeather(location);
  }, [location]);

  if (error) {
    return (
      <div style={{ color: "black" }}>
        There has been an error!
        <br />
        <button onClick={() => setError(false)}>Reset</button>
      </div>
    );
  }
  if (loading) {
    return (
      <div
        style={{
          display: "flex",
          width: "200px",
          height: "240px",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <PulseLoader size={15} color="purple" />
      </div>
    );
  }

  return (
    <WeatherCard
      temp={weather.temp}
      condition={weather.condition}
      city={weather.city}
      country={weather.country}
      getWeather={getWeather}
    />
  );
};

export default WeatherEngine;
