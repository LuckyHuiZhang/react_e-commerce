import React from "react";
//import styled from "@emotion/styled";

const LoginForm = (props) => {
  return (
    <div className="loginForm">
      <div className="header">Login</div>

      <div className="content">
        <div className="imgcontainer">
          <img className="icon" src="./img/login.png" alt="login Icon" />
        </div>

        <div className="form">
          <div className="form-group">
            <label htmlFor="username">User Name</label>
            <input type="text" name="username" placeholder="username" />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input type="password" name="password" placeholder="password" />
          </div>
        </div>
      </div>

      <div className="footer">
        <button type="button" className="btn">
          Login
        </button>
      </div>

      <div className="signUp">
        <h5>------------New to MyTrade?------------</h5>
        <button type="button" className="btn">
          Create your MyTrade account
        </button>
      </div>
    </div>
  );
};

export default LoginForm;
