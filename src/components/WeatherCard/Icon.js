import React from "react";
import styled from "@emotion/styled";

const Icon = (props) => {
  const Icon = styled.img`
    width: 40%;
  `;
  var icon = "";
  switch (props.condition) {
    case "Clear":
      icon = `./img/wi-day-sunny.svg`;
      break;
    case "Clouds":
      icon = `./img/wi-cloudy.svg`;
      break;
    case "Haze":
      icon = `./img/wi-day-haze.svg`;
      break;
    case "Hail":
      icon = `./img/wi-day-hail.svg`;
      break;
    case "Fog":
      icon = `./img/wi-day-fog.svg`;
      break;
    case "Tornado":
      icon = `./img/wi-tornado.svg`;
      break;
    case "Dust":
      icon = `./img/wi-dust.svg`;
      break;
    case "Snow":
      icon = `./img/wi-snow.svg`;
      break;
    case "Rain":
      icon = `./img/wi-rain.svg`;
      break;
    case "Thunderstorm":
      icon = `./img/wi-thunderstorm.svg`;
      break;
    default:
      break;
  }

  return <Icon src={icon} alt="Weather Icon" />;
};

export default Icon;
